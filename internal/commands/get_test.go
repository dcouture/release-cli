package commands

import (
	"bytes"
	"errors"
	"flag"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/release-cli/internal/flags"
	"gitlab.com/gitlab-org/release-cli/internal/gitlab"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
)

func TestGet(t *testing.T) {
	buf := new(bytes.Buffer)
	logger, hook := testlog.NewNullLogger()
	logger.SetOutput(buf)

	validSet := flag.NewFlagSet(t.Name(), 0)
	validSet.String(flags.ServerURL, "https://gitlab.com", "")
	validSet.String(flags.JobToken, "token", "")

	tests := map[string]struct {
		mock               *gitlab.MockHTTPClient
		flagSet            *flag.FlagSet
		clientFnErr        error
		expectedOutput     string
		expectedLogMessage string
		expectedErrorMsg   string
	}{
		"get_release_success": {
			mock: func() *gitlab.MockHTTPClient {
				mhc := &gitlab.MockHTTPClient{}
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseGetReleaseSuccess](), nil).Once()

				return mhc
			}(),
			flagSet:            validSet,
			expectedLogMessage: "Getting release",
			expectedOutput:     `"tag_name":"v0.1"`,
		},
		"http_client_error": {
			mock:               &gitlab.MockHTTPClient{},
			flagSet:            validSet,
			clientFnErr:        errors.New("client failed"),
			expectedLogMessage: "Getting release",
			expectedErrorMsg:   "http client: client failed",
		},
		"response_is_not_JSON": {
			mock: func() *gitlab.MockHTTPClient {
				mhc := &gitlab.MockHTTPClient{}
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseNotJSON](), nil).Once()

				return mhc
			}(),
			flagSet:            validSet,
			expectedLogMessage: "Getting release",
			expectedErrorMsg:   "get release: failed to decode response:",
		},
		"gitlab_client_error": {
			mock: &gitlab.MockHTTPClient{},
			flagSet: func() *flag.FlagSet {
				set := flag.NewFlagSet(t.Name(), 0)
				set.String(flags.ServerURL, "https://gitlab.com", "")

				return set
			}(),
			expectedErrorMsg: "create GitLab client: access token not provided",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			get, ctx := newTestGet(t, logger, func(ctx *cli.Context, log logrus.FieldLogger) (gitlab.HTTPClient, error) {
				return tt.mock, tt.clientFnErr
			}, tt.flagSet, "-tag-name", "v0.1")
			require.Equal(t, "get", get.Name)
			require.Len(t, get.Flags, 2)

			defer tt.mock.AssertExpectations(t)

			err := get.Run(ctx)
			if tt.expectedErrorMsg != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.expectedErrorMsg)
				return
			}

			require.NoError(t, err)
			require.Equal(t, tt.expectedLogMessage, hook.LastEntry().Message)
			require.Contains(t, buf.String(), tt.expectedOutput)
		})
	}
}

func newTestGet(t *testing.T, logger *logrus.Logger, clientFn httpClientFn, flagSet *flag.FlagSet, extraArgs ...string) (*cli.Command, *cli.Context) {
	t.Helper()

	args := []string{"get"}
	args = append(args, extraArgs...)

	app := &cli.App{Writer: logger.Out}

	err := flagSet.Parse(args)
	require.NoError(t, err)

	get := Get(logger, clientFn)
	ctx := cli.NewContext(app, flagSet, nil)

	return get, ctx
}
