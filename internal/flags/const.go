package flags

const (
	// ServerURL available in the CLI context
	ServerURL = "server-url"
	// JobToken available in the CLI context
	JobToken = "job-token"
	// PrivateToken available in the CLI context
	PrivateToken = "private-token"
	// ProjectID available in the CLI context
	ProjectID = "project-id"
	// Timeout available in the CLI context
	Timeout = "timeout"
	// Name available in the CLI context
	Name = "name"
	// Description available in the CLI context
	Description = "description"
	// TagName available in the CLI context
	TagName = "tag-name"
	// Ref available in the CLI context
	Ref = "ref"
	// AssetsLinksName available in the CLI context
	AssetsLinksName = "assets-links-name"
	// AssetsLinksURL available in the CLI context
	AssetsLinksURL = "assets-links-url"
	// AssetsLink available in the CLI context
	AssetsLink = "assets-link"
	// Milestone available in the CLI context
	Milestone = "milestone"
	// ReleasedAt available in the CLI context
	ReleasedAt = "released-at"
	// AdditionalCACertBundle available in the CLI context
	AdditionalCACertBundle = "additional-ca-cert-bundle"
	// InsecureHTTPS flag available in the CLI context
	InsecureHTTPS = "insecure-https"
	// IncludeHTMLDescription flag
	IncludeHTMLDescription = "include-html-description"
)
