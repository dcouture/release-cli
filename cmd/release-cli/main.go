package main

import (
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/release-cli/internal/app"
)

var (
	// VERSION when the binary was built
	VERSION string
)

func main() {
	log := logrus.New().WithFields(logrus.Fields{
		"cli":     "release-cli",
		"version": VERSION,
	})

	defer recovered(log)

	if err := app.New(log, VERSION).Run(os.Args); err != nil {
		log.WithError(err).Fatal("run app")
	}
}

func recovered(log logrus.FieldLogger) {
	if r := recover(); r != nil {
		log.Fatalf("recovered from panic: %+v", r)
	}
}
